package com.tataaia.omnichannel.SIS.pdfmapper.impl.utils;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import com.tataaia.omnichannel.SIS.DAO.Entity.mssql.ApplicationMainEntity;
import com.tataaia.omnichannel.SIS.DAO.Entity.mssql.LeadEntity;
import com.tataaia.omnichannel.SIS.DAO.Entity.mssql.ProductMasterEntity;
import com.tataaia.omnichannel.SIS.DAO.Entity.mssql.ProposerSubmissionEntity;
import com.tataaia.omnichannel.SIS.DAO.mssql.ApplicationMainDao;
import com.tataaia.omnichannel.SIS.DAO.mssql.LeadRepo;
import com.tataaia.omnichannel.SIS.DAO.mssql.LpUrlRepo;
import com.tataaia.omnichannel.SIS.DAO.mssql.PolicyDetailsDao;
import com.tataaia.omnichannel.SIS.DAO.mssql.ProposerDetailsRepo;
import com.tataaia.omnichannel.SIS.RestInvokerService.RestTemplateInvoker;
import com.tataaia.omnichannel.SIS.config.RestTemplateConfig;
import com.tataaia.omnichannel.SIS.exception.OmniPlatformException;
import com.tataaia.omnichannel.SIS.pojo.RequestDTO;
import com.tataaia.omnichannel.SIS.pojo.ResultPojo;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class SRSPOSMapperTest {
	
	@InjectMocks
	SRSPOSMapper srsPOSMapper;
	@Mock
	PolicyDetailsDao policyDetailsDao;
	@Mock
	ApplicationMainDao applicationMainDao;
	@Mock
	ProposerDetailsRepo proposerDetailsRepo;
	@Mock
	LeadRepo leadRepository;
	@Mock
	LpUrlRepo omniLpUrlRepo;
	@Mock
	RestTemplateInvoker restTemplateInvoker;

	@ParameterizedTest
	@CsvSource({
		"false,false,SINGLE,ADBLI,Life Plus Option",
		"false,true,ANNUALLY,HCBLI,life income option",
		"true,false,SEMIANNUALLY,TTIWP,plan",
		"false,false,SEMIANNUALLY,TTIWP,plan",
		"true,true,QUARTERLY,TTILI,plan",
		"false,false,MONTHLY,TTIPC,plan"
	})
	void mapTest(boolean isWellnessFlag,boolean employeeFlag, String paymentFreq, String flag, String planOption) throws OmniPlatformException {
		Map<Integer, Map<String, ResultPojo>> result = new HashMap<>();
		RequestDTO dto = new RequestDTO();
		dto.setPolicyHolderName("name");
		dto.setPolicyHolderAge(50);
		dto.setLifeAssuredName("name");
		dto.setInsuredAge(50);
		dto.setPolicyTerm(10);
		dto.setPpt(10);
		dto.setProposalNumber("100");
		dto.setPaymentFrequency(paymentFreq);
		dto.setSumAssured(100D);
		dto.setSmartOption(true);
		dto.setVersion(5L);
		dto.setProduct(347L);
		dto.setGender("male");
		dto.setPolicyHolderGender("male");
		dto.setEntryAge(25);
		dto.setBonus("ACB");
		dto.setPlanOption(planOption);
		dto.setPremiumPaymentMode("LP");
		dto.setSmokingStatus("status");
		dto.setIsWellness(isWellnessFlag);
		dto.setEmployeesOfPromoterGrp(employeeFlag);
		dto.setExistingToTataAIA(true);
		ResultPojo resultPojo = new ResultPojo();
		resultPojo.setResult(1);
		
		Map<String, List<Map<String, Object>>> group = new HashMap<>();
		List<Map<String, Object>> groupEntryList = new ArrayList<>();
		Map<String, Object> modelMap = new HashMap<String, Object>() {
			{
				put("riderInsured", "Insured");
				put("proposerAge", 50);
				put("insuredSex", "MALE");
				put("proposerSex", "MALE");
				put("riderCoverageTerm", 18);
				put("flag", flag);
				put("incomeFrequency", "A");
				put("riderLumpsumSumAssured", 8);
				put("riderROP", false);
				put("riderCode", "riderCode");
				put("basePlanPaymentMode", "ANNUALLY");
				put("basePolicyTerm", 18);
				put("riderDataList", resultPojo);
				put("riderInsuredName", "User test");
				put("riderSmokerFlag", "N");
			}
		};
		groupEntryList.add(modelMap);
		group.put("lumpsum", groupEntryList);
		group.put("partnerCare", groupEntryList);
		group.put("waiverOfPremium", groupEntryList);
		dto.setGroup(group);
		
		ApplicationMainEntity sis = new ApplicationMainEntity();
		sis.setSisNo(BigInteger.valueOf(0));
		when(applicationMainDao.findByAppId(Mockito.anyInt())).thenReturn(sis);
		
		ProposerSubmissionEntity proposerDetails = new ProposerSubmissionEntity();
		proposerDetails.setLeadid(100L);
		proposerDetails.setProposerOtp("otp");
		proposerDetails.setOtpValidity(Timestamp.valueOf("2022-12-05 13:13:05.990"));
		proposerDetails.setContactNo(999L);
		proposerDetails.setCountryCallCode(91);
		proposerDetails.setFirstName("name");
		proposerDetails.setMidName("name");
		proposerDetails.setLastName("name");
		when(proposerDetailsRepo.findByAppId(Mockito.anyLong())).thenReturn(proposerDetails);
		
		LeadEntity leadDets = new LeadEntity();
		when(leadRepository.findByLeadId(Mockito.anyLong())).thenReturn(leadDets);
		
		ProductMasterEntity product = new ProductMasterEntity();
		product.setDisplayName("name");
		
		Map<String, ResultPojo> res = new HashMap<>();
		ResultPojo r1 = new ResultPojo();
		r1.setResult(10);
		res.put("ServiceTaxFirstYear", r1);
		res.put("TotalInstalmentPremium_InstalmentPremiumWithFirstYearGST", r1);
		res.put("ModalPremiumForBasePlanAPdf", r1);
		res.put("ModalPremiumForRiderBPdf", r1);
		res.put("TotalInstalmentPremium_InstalmentPremiumWithoutGSTPdf", r1);
		res.put("BasePlanInstalmentPremiumWithFirstYearGSTPdf", r1);
		res.put("RiderInstalmentPremiumWithFirstYearGSTPdf", r1);
		res.put("TotalInstalmentPremium_InstalmentPremiumWithFirstYearGSTPdf", r1);
		res.put("BasePlanInstalmentPremiumWithGST2ndYearOnwardsPdf", r1);
		res.put("RiderInstalmentPremiumWithGST2ndYearOnwardsPdf", r1);
		res.put("TotalInstalmentPremium_InstalmentPremiumWithGST2ndYearOnwardsPdf", r1);
		res.put("ServiceTaxSecondYear", r1);
		res.put("DeathBenefitPdf", r1);
		res.put("DiscountedRiderPremiumTax", r1);
		res.put("TotalInstalmentPremiumPostApplicableDiscountWithFirstYearGst", r1);
		res.put("DiscountedModalPremium1YearBaseWithGST", r1);
		res.put("CurrentPolicyYear", r1);
		res.put("MaturityBenefit", r1);
		res.put("TranchPremiumOutput", r1);
		res.put("AnnualisedPremiumOutputPdf", r1);
		res.put("AnnualisedPremiumPdf", r1);
		res.put("SurvivalBenefit", r1);
		res.put("MinGuaranteedSurrenderValueLifeIncome", r1);
		res.put("MinGuaranteedSurrenderValuePdf", r1);
		res.put("PolicyCancellationValuePdf", r1);
		res.put("SSVLifeIncome", r1);
		res.put("SpecialSurrenderValue", r1);
		res.put("foobar", r1);
		result.put(0, res);
		result.put(11, res);
		assertNotNull(srsPOSMapper.map(result, dto, product));
	}

}
