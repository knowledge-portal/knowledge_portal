package com.example.knowledge.portal.controller;

import com.example.knowledge.portal.dto.VideoDto;
import com.example.knowledge.portal.entity.User;
import com.example.knowledge.portal.entity.Video;
import com.example.knowledge.portal.payloads.SearchVideos;
import com.example.knowledge.portal.service.UserService;
import com.example.knowledge.portal.service.VideoService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/video")
@AllArgsConstructor
public class VideoController {
    @Autowired
    private VideoService videoService;

    @Autowired
    private UserService userService;

    // Each parameter annotated with
// @RequestParam corresponds to a form field where the String argument is the name of the field
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/saveVideo")
    public ResponseEntity<VideoDto> saveVideo(@RequestParam("file") MultipartFile file,
                                           @RequestParam("name") String name,
                                           Principal principal) throws IOException {
        VideoDto videoDto = this.videoService.saveVideo(file, name, principal);
        return new ResponseEntity<>(videoDto, HttpStatus.CREATED);
    }

    @GetMapping("/{name}")
    public ResponseEntity<VideoDto> getVideoByName(@PathVariable("name") String name) {

        VideoDto videoDto = this.videoService.getVideo(name);
        return new ResponseEntity<>(videoDto, HttpStatus.FOUND);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/showAllVideos")
    public ResponseEntity<List<String>> getAllVideoNames() {
        List<String> allVideoNames = this.videoService.getAllVideoNames();
        return new ResponseEntity<>(allVideoNames, HttpStatus.FOUND);
    }

    @GetMapping("/searchKey/{keyword}")
    public ResponseEntity<SearchVideos> searchVideosByKeyword(@PathVariable("keyword") String keyword)
    {
        SearchVideos searchVideos = this.videoService.searchVideos(keyword);
        return new ResponseEntity<>(searchVideos, HttpStatus.FOUND);
    }
}




