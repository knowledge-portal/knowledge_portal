package com.example.knowledge.portal.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class VideoDto {

    private String name;

    private UserDto user;

    private List<CommentDto> comments;

    //private byte[] data;
}
