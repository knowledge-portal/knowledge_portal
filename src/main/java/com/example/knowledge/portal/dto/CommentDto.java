package com.example.knowledge.portal.dto;

import com.example.knowledge.portal.entity.User;
import com.example.knowledge.portal.entity.Video;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class CommentDto {

    private String content;

    private String date;

    private String time;

    private UserDto user;

}
