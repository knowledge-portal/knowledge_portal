package com.example.knowledge.portal.dto;

import com.example.knowledge.portal.entity.Video;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class UserDto {


    private String firstName;

    private String lastname;

    private String email;

    private Long mobile;

    private String designation;

    private String competency;

}
