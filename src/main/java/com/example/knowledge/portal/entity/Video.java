package com.example.knowledge.portal.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Video {



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "VIDEO_ID")
    private Long id;

    @Column(unique = true, name = "VIDEO_NAME")
    private String name;

    @Column(name = "VIDEO_CATEGORY")
    private String category;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "video")
    private List<Comment> comments = new ArrayList<>();
    @Lob
    private byte[] data;

    public Video(String name, byte[] data) {
        this.name = name;
        this.data = data;
    }


}
