package com.example.knowledge.portal.service;

import com.example.knowledge.portal.dao.CommentRepo;
import com.example.knowledge.portal.dao.UserRepo;
import com.example.knowledge.portal.dao.VideoRepo;
import com.example.knowledge.portal.dto.CommentDto;
import com.example.knowledge.portal.entity.Comment;
import com.example.knowledge.portal.entity.User;
import com.example.knowledge.portal.entity.Video;
import com.example.knowledge.portal.exception.ResourceNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class CommentServiceImpl implements CommentService{

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CommentRepo commentRepo;

    @Autowired
    private VideoRepo videoRepo;

    @Autowired
    private UserRepo userRepo;

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yy");

    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");

    @Override
    public CommentDto addComment(Comment comment, Long videoId, Principal principal) {
        User user = this.userRepo.getUserByUserName(principal.getName());
        Video video = this.videoRepo.findById(videoId).
                orElseThrow(() -> new ResourceNotFoundException("Video", "Id", String.valueOf(videoId)));
        comment.setDate(dateFormat.format(new Date()));
        comment.setTime(timeFormat.format(new Date()));
        comment.setUser(user);
        comment.setVideo(video);
        Comment newComment = this.commentRepo.save(comment);
        return this.modelMapper.map(newComment, CommentDto.class);

    }

    @Override
    public void deleteComment(Long commentId) {
        Comment comment = this.commentRepo.findById(commentId).
                orElseThrow(() -> new ResourceNotFoundException("Comment", "Id", String.valueOf(commentId)));
    }

    @Override
    public CommentDto updateComment(Comment comment) {
        return null;
    }
}
