package com.example.knowledge.portal.service;

import com.example.knowledge.portal.dto.VideoDto;
import com.example.knowledge.portal.entity.Video;
import com.example.knowledge.portal.payloads.SearchVideos;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

public interface VideoService {
    VideoDto getVideo(String name);

    VideoDto saveVideo(MultipartFile file, String name, Principal principal) throws IOException;

    List<String> getAllVideoNames();

    SearchVideos searchVideos(String keyword);
}
