package com.example.knowledge.portal.service;

import com.example.knowledge.portal.dto.CommentDto;
import com.example.knowledge.portal.entity.Comment;

import java.security.Principal;

public interface CommentService {

    CommentDto addComment(Comment comment, Long videoId, Principal principal);

    void deleteComment(Long commentId);

    CommentDto updateComment(Comment comment);
}
