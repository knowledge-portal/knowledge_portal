package com.example.knowledge.portal.service;

import com.example.knowledge.portal.dto.UserDto;
import com.example.knowledge.portal.entity.User;
import com.example.knowledge.portal.payloads.ApiResponse;

import java.util.List;

public interface UserService {

    UserDto addUser(User user);

    UserDto updateUserDetails(User user);

    ApiResponse deleteUser(Long userId);

    UserDto getUserById(Long userId);

    List<UserDto> getAllUsers();

    UserDto getUserByUserName(String username);
}
