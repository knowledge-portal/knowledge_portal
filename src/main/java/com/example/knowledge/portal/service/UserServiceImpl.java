package com.example.knowledge.portal.service;

import com.example.knowledge.portal.dao.UserRepo;
import com.example.knowledge.portal.dto.UserDto;
import com.example.knowledge.portal.entity.User;
import com.example.knowledge.portal.exception.ResourceNotFoundException;
import com.example.knowledge.portal.payloads.ApiResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private ModelMapper modelMapper;
    @Override
    public UserDto addUser(User user) {
        user.setRole("ROLE_USER");
        user.setPassword(this.passwordEncoder.encode(user.getPassword()));
        User newUser = this.userRepo.save(user);

        return this.modelMapper.map(newUser, UserDto.class);
    }

    @Override
    public UserDto updateUserDetails(User user) {
        user.setRole("ROLE_USER");
        User updatedUser = this.userRepo.save(user);
        return this.modelMapper.map(updatedUser, UserDto.class);
    }

    @Override
    public ApiResponse deleteUser(Long userId) {
        User user = this.userRepo.findById(userId).
                orElseThrow(() -> new ResourceNotFoundException("User", "Id", String.valueOf(userId)));
        this.userRepo.delete(user);

        String message = "User deleted successfully";
        return new ApiResponse(message, true);
    }

    @Override
    public List<UserDto> getAllUsers() {
        List<User> allUsers =  this.userRepo.findAll();
        List<UserDto> allUserDtos = new ArrayList<>();

        for(User user : allUsers)
        {
            allUserDtos.add(this.modelMapper.map(user, UserDto.class));
        }
        return allUserDtos;
    }

    @Override
    public UserDto getUserByUserName(String username) {
        User user = this.userRepo.getUserByUserName(username);
        return this.modelMapper.map(user, UserDto.class);
    }

    @Override
    public UserDto getUserById(Long userId) {
        User user = this.userRepo.findById(userId).
                orElseThrow(() -> new ResourceNotFoundException("User", "Id", String.valueOf(userId)));
        return this.modelMapper.map(user, UserDto.class);
    }
}
