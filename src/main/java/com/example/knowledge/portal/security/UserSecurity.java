package com.example.knowledge.portal.security;

import com.example.knowledge.portal.dao.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component("userSecurity")
public class UserSecurity {

    @Autowired
    private UserRepo userRepo;

    public boolean hasUserId(Authentication authentication, Long userId)
    {
        Long userID = this.userRepo.getUserByUserName(authentication.getName()).getUserId();
        if(userId.equals(userID))
            return true;
        return false;
    }
}
